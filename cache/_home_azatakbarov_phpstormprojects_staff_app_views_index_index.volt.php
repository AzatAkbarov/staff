<div class="page-header">
    <?php if ($auth) { ?>
        <a class="btn btn-danger pull-right" href="<?= $this->url->get('logout') ?>" title="logout">Выход</a>
        <a class="btn btn-primary pull-right" href="<?= $this->url->get('user/changePassword') ?>">Сменить пароль</a>
    <?php } else { ?>
        <a class="btn btn-success pull-right" href="<?= $this->url->get('login') ?>" title="login">Авторизоваться</a>
    <?php } ?>
</div>
<p>Система стафф</p>

<p>Количество отработанных часов: </p>
<p>Выполнено работы: </p>
<p>Всего нужно отработать: </p>

<div style="text-align: center">
    <form id="search">
        <?= $form->render('month') ?>
        <?= $form->render('year') ?>
    </form>
</div>
<br>
<table class="table table-bordered table-striped" align="center">
    <tr>
        <th style="width: 10px"><a href="#">hide/show</a></th>
        <?php foreach ($users as $user) { ?>
            <th style="text-align: center"><?= $user['name'] ?></th>
        <?php } ?>
    </tr>
    <?php foreach ($data as $key => $value) { ?>
        <tr>
            <td style="text-align: center"><span><?= $key ?></span><br>
                <div style="border: solid black 1px; text-align: center"><?= $value[0] ?></div>
            </td>
            <?php foreach ($users as $user) { ?>
                <td style="min-width: 110px">
                    <div class="item-header">
                        <input style="display: inline-block" class="checkbox" type="checkbox"
                               disabled <?php if (($value[0] !== 'Saturday' && $value[0] !== 'Sunday')) { ?> checked="checked" <?php } ?>>
                        <?php if (($user['id'] == $id && $value[1] == $currentDate)) { ?>
                        <span id="work-status"
                              class="btn <?php if ($button === 'Start') { ?> btn-success <?php } else { ?> btn-danger <?php } ?> pull-right">
                            <?= $button ?>
                        </span>
                        <?php } ?>
                    </div>
                    <?php foreach ($days as $day) { ?>
                        <?php if ($day['date'] == $value[1] && $user['id'] === $day['user_id']) { ?>
                                <div id=<?= $day['id'] ?> <?php if ($day['late']) { ?> class="day-late" <?php } ?>>
                                    <?php foreach ($day['times'] as $time) { ?>
                                        <br><span><?= $time['start'] ?></span> - <span><?= $time['end'] ?></span>
                                    <?php } ?>
                                </div>
                                <hr>
                                <p><?= $day['total'] ?></p>
                        <?php } ?>
                    <?php } ?>

                </td>
            <?php } ?>
        </tr>
    <?php } ?>
</table>
<script type="text/javascript">
    $(function () {
        $('.day-late').parent().addClass('td-late');
        let button = $('#work-status');
        button.on('click', function () {
            $.ajax({
                method: "POST",
                url: "/user/startStopWork",
            }).done(function (response) {
                let id = response.day_id;
                let parentTd = $(button).parent();
                if(response.is_late === 1){
                    parentTd.parent().addClass('td-late')
                }
                if(response.is_new){
                    let newDiv = $(`<div id="${id}"></div><hr><p>00:00:00</p>`);
                    parentTd.parent().append(newDiv);
                }
                let div = $(`#${id}`);
                button.text(response.button);
                if (response.button === 'Start') {
                    button.removeClass('btn-danger').addClass('btn-success');
                    div.append(`<span>${response.time['end']}</span>`);
                } else {
                    div.append(`<br><span>${response.time['start']}</span> - `);
                    button.removeClass('btn-success').addClass('btn-danger');
                }
            });
        });

        let form = $('#search');
        form.on('change', function () {

            let month = $('#month').val();
            let year = $('#year').val();
            $.ajax({
                method: "POST",
                url: "/",
                data: {
                    month: month,
                    year: year
                }
            }).done(function () {
                location.reload()
            }).fail(function () {
                alert('Извините, произошла ошибка!')
            });
        })
    });
</script>