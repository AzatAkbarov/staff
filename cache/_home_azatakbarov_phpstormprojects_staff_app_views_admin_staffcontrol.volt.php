<div class="page-header">
    <?php if ($auth) { ?>
        <a class="btn btn-danger pull-right" href="<?= $this->url->get('logout') ?>" title="logout">Выход</a>
    <?php } else { ?>
        <a class="btn btn-success pull-right" href="<?= $this->url->get('login') ?>" title="login">Авторизоваться</a>
    <?php } ?>
</div>
<p>Контроль системы</p>

<p>Количество отработанных часов: </p>
<p>Выполнено работы: </p>
<p>Всего нужно отработать: </p>

<div style="text-align: center">
    <form id="staff_search">
        <?= $form->render('month') ?>
        <?= $form->render('year') ?>
    </form>
</div>
<br>
<table class="table table-bordered table-striped" align="center">
    <tr>
        <th style="width: 10px"><a href="#">hide/show</a></th>
        <?php foreach ($users as $user) { ?>
            <th style="text-align: center"><?= $user['name'] ?></th>
        <?php } ?>
    </tr>
    <?php foreach ($data as $key => $value) { ?>
        <tr>
            <td style="text-align: center"><span><?= $key ?></span><br>
                <div style="border: solid black 1px; text-align: center"><?= $value[0] ?></div>
            </td>
            <?php foreach ($users as $user) { ?>
                <td style="min-width: 110px">
                    <div class="item-header">
                        <input style="display: inline-block" class="checkbox" type="checkbox"
                               <?php if (($value[0] !== 'Saturday' && $value[0] !== 'Sunday')) { ?> checked="checked" <?php } ?>>
                        <?php if (($value[1] == $currentDate)) { ?>
                            <input style="display: inline-block" class="checkbox" type="checkbox"
                                   <?php if (($value[0] !== 'Saturday' && $value[0] !== 'Sunday')) { ?> checked="checked" <?php } ?>>
                        <?php } ?>
                    </div>
                    <?php foreach ($user['days'] as $day) { ?>
                        <?php if ($day['date'] == $value[1] && $user['id'] === $day['user_id']) { ?>
                            <?php if (($value[1] == $currentDate)) { ?>
                                <span data-user-id="<?= $user['id'] ?>" class="work-status btn <?php if ($day['button'] === 'Start') { ?> btn-success <?php } else { ?> btn-danger <?php } ?> pull-right">
                                    <?= $day['button'] ?>
                                </span>
                            <?php } ?>
                            <div id=<?= $day['id'] ?>>
                                <?php foreach ($day['times'] as $time) { ?>
                                    <br><input data-time-id="<?= $time['id'] ?>" data-type="start" class="time-input" value="<?= $time['start'] ?>"> - <input data-time-id="<?= $time['id'] ?>" data-type="end" class="time-input" value="<?= $time['end'] ?>">
                                <?php } ?>
                            </div>
                            <hr>
                            <p><?= $day['total'] ?></p>
                        <?php } ?>
                    <?php } ?>
                </td>
            <?php } ?>
        </tr>
    <?php } ?>
</table>
<script type="text/javascript">
    $(function () {

        $('.time-input').on('change', function () {
            let data = ($(this).data());
            let value = $(this).val();

            if(!value.match("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")) {
                swal.fire({
                    type: 'error',
                    title: 'Ошибка!Введенные данные не соответствуют формату часов!',
                    timer: 5000
                });
            } else {
                console.log('good')
            }

        });

        let form = $('#staff_search');
        form.on('change', function () {

            let month = $('#month').val();
            let year = $('#year').val();
            $.ajax({
                method: "POST",
                url: "/admin/staffControl",
                data: {
                    month: month,
                    year: year
                }
            }).done(function () {
                location.reload()
            }).fail(function () {
                alert('Извините, произошла ошибка!')
            });
        })
    });
</script>