<?php

namespace App\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class UserCreateForm extends Form
{

    public function initialize()
    {
        $name = new Text('name', [
            'placeholder' => 'Имя пользователя'
        ]);

        $name->addValidators([
            new PresenceOf([
                'message' => 'Введите имя пользователя!'
            ])
        ]);

        $this->add($name);

        $login = new Text('login', [
            'placeholder' => 'Логин пользователя'
        ]);

        $login->addValidators([
            new PresenceOf([
                'message' => 'Введите логин пользователя!'
            ])
        ]);

        $this->add($login);


        // Password
        $password = new Password('password', [
            'placeholder' => 'Пароль'
        ]);

        $password->addValidator(new PresenceOf([
            'message' => 'Введите пароль!'
        ]));

        $password->clear();

        $this->add($password);

        // Email
        $email = new Text('email', [
            'placeholder' => 'Email'
        ]);

        $email->addValidators([
            new PresenceOf([
                'message' => 'Введите email!'
            ]),
            new Email([
                'message' => 'Email не корректен!'
            ])
        ]);

        $this->add($email);

        // Email
        $role = new Text('email', [
            'placeholder' => 'Email'
        ]);

        $role->addValidators([
            new PresenceOf([
                'message' => 'Введите email!'
            ]),
            new Email([
                'message' => 'Email не корректен!'
            ])
        ]);

        $this->add($role);



        $this->add(new Submit('save', [
            'class' => 'btn btn-primary',
            'value' => 'Сохранить'
        ]));
    }
}