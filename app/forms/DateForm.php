<?php

namespace App\Forms;

use App\Models\Day;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Submit;


class DateForm extends Form
{

    /**
     * @throws \Exception
     */
    public function initialize()
    {
        $current_date = Day::getNewDateTime();

        $current_month = ((int)$current_date->format('m'));
        $month_difference = $current_month - 1;

        $beginning_of_the_year = $current_date->modify("-{$month_difference} month");

        $month_options = [];
        for ($i = 1; $i <= 12; $i++) {
            $month_options[$i] = $beginning_of_the_year->format('F');
            $beginning_of_the_year->modify("+1 month");
        }

        $month = new Select('month', $month_options);

        $month->setDefault($current_month);
        $this->add($month);

        $current_year = (int)$current_date->format('Y');

        $year_options = [];
        $j = 10;

        for ($i = 1; $i <= 10; $i++) {
            $year_options[$current_year - $j] = $current_year - $j;
            $j--;
        }
        $year = new Select('year', $year_options);

        $year->setDefault(Day::getNewDateTime()->format('Y'));
        $this->add($year);

        $this->add(new Submit('save', [
            'class' => 'btn btn-primary',
            'value' => 'Сохранить'
        ]));
    }
}