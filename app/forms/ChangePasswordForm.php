<?php

namespace App\Forms;

use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class ChangePasswordForm extends Form
{

    public function initialize()
    {
        // Password
        $password = new Password('password', [
            'placeholder' => 'Новый пароль'
        ]);


        $password->addValidators([
            new PresenceOf([
                'message' => 'Необходимо ввести пароль!'
            ]),
            new StringLength([
                'min' => 8,
                'messageMinimum' => 'Пароль слишком короткий,должно быть как минимум 8 символов!'
            ]),
            new Confirmation([
                'message' => 'Пароль не соответствует подтвержденному паролю!',
                'with' => 'confirmPassword'
            ])
        ]);

        $this->add($password);

        // Confirm Password
        $confirmPassword = new Password('confirmPassword', [
            'placeholder' => 'Подтвердите пароль!'
        ]);

        $confirmPassword->addValidators([
            new PresenceOf([
                'message' => 'Требуется пароль для подтверждения'
            ])
        ]);

        $this->add($confirmPassword);
        $this->add(new Submit('save', [
            'class' => 'btn btn-primary',
            'value' => 'Сохранить'
        ]));
    }
}
