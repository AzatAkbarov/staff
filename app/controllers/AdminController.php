<?php

namespace App\Controllers;

use App\Forms\DateForm;
use App\Forms\UserCreateForm;
use App\Models\Day;
use App\Models\Time;
use App\Models\User;
use App\Models\Holiday;

class AdminController extends ControllerBase
{
    public function showUsersAction()
    {
//        $this->view->setTemplateBefore('adminLayout');
        $users = User::find();
        $this->view->setVar('users', $users);
    }

    public function deleteUserAction($id)
    {
        $user = User::findFirst($id);
        $ban = $user->getBaned();
        $ban == 1 ? $ban = 0 : $ban = 1;
        $user->update($user->setBaned($ban));
        return $this->response->redirect(
            "/admin/users/show"
        );
    }


    public function createHolidayAction()
    {
        if ($this->request->isPost()) {
            $title = $this->request->getPost('title');
            $date = $this->request->getPost('date');
            $is_every_year = (int)$this->request->getPost('is_every_year');

            try {
                $date = date_create_from_format('j/M/Y', $date);
            } catch (\Exception $e) {
                return $this->response->setStatusCode(500);
            }

            $holiday = new Holiday();
            $holiday->title = $title;
            $holiday->date = $date->format('Y-m-d H:i:s');
            $holiday->is_every_year = $is_every_year;

            try {
                if ($holiday->validation()) {
                    $holiday->save();
                    return $this->response->setJsonContent(
                        [
                            'id' => $holiday->id,
                            'title' => $holiday->title,
                            'date' => $holiday->date,
                            'status' => $holiday->is_every_year
                        ]
                    );
                } else {
                    return $this->response->setJsonContent(
                        [
                            'message' => 'валидация не прошла',
                            'holiday' => $holiday
                        ]
                    );
                }
            } catch (\Exception $e) {
                return $this->response->setJsonContent(
                    [
                        'message' => 'что то не так',
                        'holiday' => $holiday
                    ]
                );
            }
        }
    }

    public function showHolidaysAction()
    {
        $holidays = Holiday::find();
        $this->view->holidays = $holidays;
    }

    /**
     * @throws \Exception
     */
    public function staffControlAction()
    {

        $dateForm = new DateForm();

        if ($this->session->get('staff_form_date')) {
            $date = $this->session->get('staff_form_date');
            $this->session->remove('staff_form_date');
            $dateForm->get('month')->setDefault((int)($date->format('m')));
            $dateForm->get('year')->setDefault($date->format('Y'));
        } else {
            $date = Day::getNewDateTime();
        }

        $stringDate = Day::getNewDateTime()->format('d:m:Y');

        if ($this->request->isPost()) {
            $data = $this->request->getPost();
            $month = $data['month'];
            $year = $data['year'];
            $form_date = Day::createDateTimeFromString("{$year}-{$month}-01 00:00:00");
            $this->session->set('staff_form_date', $form_date);
            return $this->response->setStatusCode(200);
        }

        $currentUser = $this->auth->getUser();

        $allUsers = User::find()->toArray();

        $users = [];

        foreach ($allUsers as $key => $user) {
            $days = Day::findAllByUserIdAndDate($date, $user['id']);
            $arrayOfDayData = [];
            foreach ($days as $daykey => $day) {
                $arrayOfDayData[$daykey] = $day->toArray();
                $lastTime = $day->time->getLast();
                $array = [];
                $button = 'Start';
                if ($lastTime) {
                    if (!$lastTime->end) {
                        $button = 'Stop';
                    }
                    $array = [];
                    foreach ($day->time as $time) {
                        $array[] = $time->getArrayData();
                    }
                }
                $arrayOfDayData[$daykey]['times'] = $array;
                $dateSting = \DateTime::createFromFormat('Y-m-d H:i:s', $day->date)->format('d:m:Y');
                $arrayOfDayData[$daykey]['date'] = $dateSting;
                $arrayOfDayData[$daykey]['total'] = Time::getTotalTime($day->id);
                $arrayOfDayData[$daykey]['button'] = $button;
            }
            $user['days'] = $arrayOfDayData;
            $users[] = $user;
        }

        $number_of_days_in_a_month = cal_days_in_month(CAL_GREGORIAN, (int)$date->format('m'), (int)$date->format('Y'));
        $first_day_in_a_month = $date->modify('first day of this month');

        $array = [];
        for ($i = 1; $i <= $number_of_days_in_a_month; $i++) {
            $array[$i] = [$first_day_in_a_month->format('l'), $first_day_in_a_month->format('d:m:Y')];
            $first_day_in_a_month->modify("+1 day");
        }

        $this->view->number = $number_of_days_in_a_month;
        $this->view->form = $dateForm;
        $this->view->setVar('auth', $currentUser);
        $this->view->setVar('data', $array);
        $this->view->currentDate = $stringDate;
        $this->view->days = $arrayOfDayData;
        $this->view->setVar('users', $users);

    }

    public function editTimeAction()
    {
        if ($this->request->isPost()) {
            $timeId = (int)$this->request->getPost('timeId');
            $property = $this->request->getPost('type');
            $value = $this->request->getPost('value');
            
            $time = Time::findFirst($timeId);
            if($time) {
                if($property === 'start') {
                    $newStringTime = substr_replace($time->start, $value,11, 5);
                    $time->start = $newStringTime;
                }
            }
            return $this->response->setJsonContent(
                [
                    'time' => $time
                ]
            );
        }
    }


    public function deleteHolidayAction($id)
    {
        try {
            $holiday = Holiday::findFirst($id);
            $holiday->delete();
            return $this->response->setJsonContent(
                [
                    'id' => $id,
                ]
            );
        } catch (\Exception $e) {
            return $this->response->setStatusCode(500);
        }
    }

    public function createUserAction()
    {
        $user = new User();
        $form = new UserCreateForm($user);
        try {
            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {
                    if ($user->validation()) {
                        $password_hash = $this->security->hash($user->getPassword());
                        $user->setPassword($password_hash);
                        $user->save();
                        return $this->response->redirect(
                            "admin/users/show"
                        );
                    } else {
                        $message = $user->getMessages();
                        $this->flash->error($message);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->flash->error($e->getMessage());
        }
        $this->view->form = $form;
    }

}