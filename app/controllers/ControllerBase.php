<?php

namespace App\Controllers;

use App\Library\Auth\Auth;
use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

/**
 * ControllerBase
 * This is the base controller for all controllers in the application
 *
 * @property Auth auth
 */
class ControllerBase extends Controller
{
    /**
     * Execute before the router so we can determine if this is a private controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();

        if ($this->acl->isPrivate($controllerName)) {

            $identity = $this->auth->getIdentity();

            if (!is_array($identity)) {

                $this->flash->notice('Вы не авторизованы!');

                $dispatcher->forward([
                    'controller' => 'session',
                    'action' => 'login'
                ]);
                return false;
            }

            $actionName = $dispatcher->getActionName();
            if (!$this->acl->isAllowed($identity['role'], $controllerName, $actionName)) {
                $this->flash->notice('Доступ закрыт!Недостаточно привилегий!');
                $dispatcher->forward([
                    'controller' => 'session',
                    'action' => 'login'
                ]);
                return false;
            }
        }
    }
}