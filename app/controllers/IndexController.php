<?php

namespace App\Controllers;

use App\Forms\DateForm;
use App\Models\Day;
use App\Models\Time;
use App\Models\User;

class IndexController extends ControllerBase
{
    /**
     * @throws \Exception
     */
    public function indexAction()
    {
        $dateForm = new DateForm();
        if($this->session->get('form_date')){
            $date = $this->session->get('form_date');
            $this->session->remove('form_date');
            $dateForm->get('month')->setDefault((int)($date->format('m')));
            $dateForm->get('year')->setDefault($date->format('Y'));
        } else {
            $date = Day::getNewDateTime();
        }

        $stringDate = Day::getNewDateTime()->format('d:m:Y');

        if($this->request->isPost()) {
            $data = $this->request->getPost();
            $month = $data['month'];
            $year = $data['year'];
            $form_date = Day::createDateTimeFromString("{$year}-{$month}-01 00:00:00");
            $this->session->set('form_date', $form_date);
            return $this->response->setStatusCode(200);
        }

        $currentUser = $this->auth->getUser();
        $allUsers = User::find()->toArray();
        $button = null;
        if ($currentUser) {
            foreach ($allUsers as $key => $user) {
                if ($user['id'] === $currentUser->getId()) {
                    unset($allUsers[$key]);
                    break;
                }
            }
            array_unshift($allUsers, $currentUser->toArray());
            $button = 'Start';
            /** @var Day $lastDayOfUser */
            $lastDayOfUser = $currentUser->day->getLast();
            if ($lastDayOfUser) {
                $lastActionDate = Day::createDateTimeFromString($lastDayOfUser->date);
                $lastActionDateString = $lastActionDate->format('d:m:Y');
                if ($lastActionDateString === $stringDate) {
                    $lastTime = $lastDayOfUser->time->getLast();
                    if (!$lastTime->end) {
                        $button = 'Stop';
                    }
                }
            }
        }

        $arrayOfDayData = [];
        $days = Day::findForDate($date);
        foreach ($days as $key => $day) {
            $arrayOfDayData[$key] = $day->toArray();
            $lastTime = $day->time->getLast();
            $array = [];
            if ($lastTime) {
                $array = [];
                foreach ($day->time as $time) {
                    $array[] = $time->getArrayData();
                }
            }
            $arrayOfDayData[$key]['times'] = $array;
            $dateSting = \DateTime::createFromFormat('Y-m-d H:i:s', $day->date)->format('d:m:Y');
            $arrayOfDayData[$key]['date'] = $dateSting;
            $arrayOfDayData[$key]['total'] = Time::getTotalTime($day->id);
        }

        $number_of_days_in_a_month = cal_days_in_month(CAL_GREGORIAN, (int)$date->format('m'), (int)$date->format('Y'));
        $first_day_in_a_month = $date->modify('first day of this month');

        $array = [];
        for ($i = 1; $i <= $number_of_days_in_a_month; $i++) {
            $array[$i] = [$first_day_in_a_month->format('l'), $first_day_in_a_month->format('d:m:Y')];
            $first_day_in_a_month->modify("+1 day");
        }

        $this->view->button = $button;
        $this->view->number = $number_of_days_in_a_month;
        $this->view->form = $dateForm;
        $this->view->setVar('auth', $currentUser);
        $this->view->setVar('data', $array);
        $this->view->currentDate = $stringDate;
        $this->view->days = $arrayOfDayData;
        if ($currentUser) {
            $this->view->id = $currentUser->getId();
        } else {
            $this->view->id = false;
        }
        $this->view->setVar('users', $allUsers);
    }

}

