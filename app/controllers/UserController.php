<?php

namespace App\Controllers;

use App\Forms\ChangePasswordForm;
use App\Models\Day;
use App\Models\Time;


class UserController extends ControllerBase
{
    /**
     * Users must use this action to change its password
     */
    public function changePasswordAction()
    {
        $form = new ChangePasswordForm();

        if ($this->request->isPost()) {
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {
                $user = $this->auth->getUser();
                $new_password = $this->security->hash($this->request->getPost('password'));
                if ($user->update($user->setPassword($new_password))) {
                    $this->flash->success('Вы успешно сменили пароль!');
                } else {
                    $message = $user->getMessages();
                    $this->flash->error($message);
                }
            }
        }
        $this->view->form = $form;
    }

    /**
     * @throws \Exception
     */
    public function startStopWorkAction()
    {

        $currentUser = $this->auth->getUser();
        $currentDay = Day::getNewDateTime();

        $data = [
            'user_id' => $currentUser->getId(),
            'date' => $currentDay
        ];

        $day = Day::findFirstByUserIdAndDate($data);

        $answer = 'Stop';
        $new = false;

        if (!$day) {
            $day = new Day();
            $day->user_id = $currentUser->getId();
            $day->date = Day::getNewDateTimeString();
            $day->beforeSave();
            $day->save();
            $time = new Time();
            $time->day_id = $day->id;
            $time->start = $day->date;
            $time->save();
            $new = true;
        } else {
            /** @var Time $lastTime */
            $lastTime = $day->time->getLast();
            if ($lastTime->end) {
                $time = new Time();
                $time->day_id = $day->id;
                $time->start = Day::getNewDateTimeString();
                $time->save();
            } else {
                $lastTime->end = Day::getNewDateTimeString();
                $diff = $lastTime->getDiff();
                $lastTime->total_time = $diff->format('%H:%i:%s');
                $lastTime->update();
                $answer = 'Start';
            }
        }

        $last = $day->time->getLast()->getArrayData();

        return $this->response->setJsonContent(
            [
                'day_id' => $day->id,
                'button' => $answer,
                'time' => $last,
                'total' => Time::getTotalTime($day->id),
                'is_new' => $new,
                'is_late' => $day->late
            ]
        );
    }
}
