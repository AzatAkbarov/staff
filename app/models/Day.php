<?php

namespace App\Models;

class Day extends \Phalcon\Mvc\Model
{


    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var \DateTime
     */
    public $date;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var integer
     */
    public $full_day;

    /**
     *
     * @var integer
     */
    public $late;

    public static function findForDate(\DateTime $date)
    {
        $query = Day::find(
            [
                'Year(date) = :year: AND Month(date) = :month:',
                'bind' => [
                    'year' => $date->format('Y'),
                    'month' => $date->format('m'),
                ],
            ]
        );
        return $query ?? false;
    }

    public static function findFirstByUserIdAndDate($data)
    {
        /** @var \DateTime $date */
        $date = $data['date'];
        $user_id = $data['user_id'];
        $query = Day::findFirst(
            [
                'Date(date) = :date: AND user_id = :user_id:',
                'bind' => [
                    'date' => $date->format('Y-m-d'),
                    'user_id' => $user_id
                ],
            ]
        );
        return $query ?? false;
    }

    public static function findAllByUserIdAndDate($date, $id)
    {
        $query = Day::find(
            [
                'Year(date) = :year: AND Month(date) = :month: AND user_id = :id:',
                'bind' => [
                    'year' => $date->format('Y'),
                    'month' => $date->format('m'),
                    'id' => $id
                ],
            ]
        );
        return $query ?? false;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("staff");
        $this->setSource("day");
        $this->hasMany('id', 'App\Models\Time', 'day_id', ['alias' => 'Time']);
        $this->belongsTo('user_id', 'App\Models\User', 'id', ['alias' => 'User']);
    }

    /**
     * @throws \Exception
     */
    public static function getNewDateTime() {
       $day = new \DateTime('NOW',new \DateTimeZone('Asia/Bishkek'));
       return $day;
    }


    /**
     * @throws \Exception
     */
    public static function getNewDateTimeString() {
        $day = new \DateTime('NOW',new \DateTimeZone('Asia/Bishkek'));
        return $day->format('Y-m-d H:i:s');
    }

    public static function createDateTimeFromString($stringDate)
    {
        $date = null;
        if($stringDate) {
            $date = \DateTime::createFromFormat('Y-m-d H:i:s', $stringDate);
        }
        return $date;
    }

    /**
     * @throws \Exception
     */
    public function beforeSave()
    {
        /** @var \DateTime $date */
        $date = self::createDateTimeFromString($this->date);
        if ($date->getTimestamp() >= self::getStartWorkingDay()->getTimestamp())
        {
            $this->late = 1;
        } else {
            $this->late = null;
        }
    }

    /**
     * @throws \Exception
     */
    public static function getStartWorkingDay()
    {
        $day = self::getNewDateTime();
        $day->setTime(9,00);
        return $day;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'day';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Day[]|Day|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Day|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
