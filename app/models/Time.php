<?php

namespace App\Models;

class Time extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $start;

    /**
     *
     * @var string
     */
    public $end;

    /**
     *
     * @var string
     */
    public $total_time;

    /**
     *
     * @var integer
     */
    public $day_id;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("staff");
        $this->setSource("time");
        $this->belongsTo('day_id', 'App\Models\Day', 'id', ['alias' => 'Day']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'time';
    }

    public function getDiff()
    {
        $start = Day::createDateTimeFromString($this->start);
        $end = Day::createDateTimeFromString($this->end);
        $diff = $end->diff($start);
        return $diff;
    }

    public function getArrayData()
    {
        $start = Day::createDateTimeFromString($this->start);
        $start = $start->format('H:i');
        $end = null;
        if($this->end){
            $end = Day::createDateTimeFromString($this->end);
            $end = $end->format('H:i');
        }
        return [
            'id' => $this->id,
            'start' => $start,
            'end' => $end
        ];
    }


    public static function getTotalTime($day_id)
    {
        $query = self::query()
            ->columns(['sec_to_time(sum(time_to_sec(total_time)))'])
            ->where('day_id = :day_id:')
            ->bind(['day_id' => $day_id])
            ->execute();

        return $query->toArray()[0]->toArray()[0] ?? false;

    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Time[]|Time|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Time|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
