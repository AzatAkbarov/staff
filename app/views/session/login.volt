{{ content() }}

<div align="center" class="well">

    {{ form('class': 'form-search') }}

    <div align="left">
        <h2>Страница авторизации</h2>
    </div>

    {{ form.render('email') }} <br>
    {{ form.render('password') }} <br>
    {{ form.render('go') }}

    {{ form.render('csrf', ['value': security.getToken()]) }}

</div>