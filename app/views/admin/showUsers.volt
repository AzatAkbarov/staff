{{ content() }}
{{ stylesheet_link('css/style.css') }}
<div align="center" class="well">

    <div align="left">
        <h2>Просмотр всех пользователей</h2>
        <h4><a class="btn btn-primary pull-right" href="{{ url("admin/user/create") }}" title="create" >Создать пользователя</a></h4>
    </div>

    <table class="table table-bordered table-striped" align="center">
        <tr>
            <th>Имя пользователя</th>
            <th>Логин</th>
            <th>Email</th>
            <th><span style="color: green">Активный</span>/<span style="color: red">Неактивный</span></th>
            <th>Действие</th>
        </tr>
        {% for user in users %}
            <tr>
                <td>{{ user.name }}</td>
                <td>{{ user.login }}</td>
                <td>{{ user.email }}</td>
                <td style="text-align: center"><span class="glyphicon glyphicon-user {{ user.baned == 1 ? 'icon-inactiv' : 'icon-activ' }}"></span></td>
                <td><a class="btn {{ user.baned == 1 ? 'btn-success' : 'btn-danger' }} pull-right" href="{{ url("admin/user/delete/" ~ user.id ) }}"> {{ user.baned == 1 ? 'Разбанить' : 'Забанить' }} </a></td>
            </tr>
        {% endfor %}
    </table>
    <hr>
</div>