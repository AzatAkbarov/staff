<div class="page-header">
    {% if auth %}
        <a class="btn btn-danger pull-right" href="{{ url("logout") }}" title="logout">Выход</a>
    {% else %}
        <a class="btn btn-success pull-right" href="{{ url("login") }}" title="login">Авторизоваться</a>
    {% endif %}
</div>
<p>Контроль системы</p>

<p>Количество отработанных часов: </p>
<p>Выполнено работы: </p>
<p>Всего нужно отработать: </p>

<div style="text-align: center">
    <form id="staff_search">
        {{ form.render('month') }}
        {{ form.render('year') }}
    </form>
</div>
<br>
<table class="table table-bordered table-striped" align="center">
    <tr>
        <th style="width: 10px"><a href="#">hide/show</a></th>
        {% for user in users %}
            <th style="text-align: center">{{ user['name'] }}</th>
        {% endfor %}
    </tr>
    {% for key,value in data %}
        <tr>
            <td style="text-align: center"><span>{{ key }}</span><br>
                <div style="border: solid black 1px; text-align: center">{{ value[0] }}</div>
            </td>
            {% for user in users %}
                <td style="min-width: 110px">
                    <div class="item-header">
                        <input style="display: inline-block" class="checkbox" type="checkbox"
                               {% if (value[0] !== 'Saturday' and value[0] !== 'Sunday') %} checked="checked" {% endif %}>
                        {% if (value[1] == currentDate) %}
                            <input style="display: inline-block" class="checkbox" type="checkbox"
                                   {% if (value[0] !== 'Saturday' and value[0] !== 'Sunday') %} checked="checked" {% endif %}>
                        {% endif %}
                    </div>
                    {% for day in user['days'] %}
                        {% if day['date'] == value[1] and user['id'] === day['user_id'] %}
                            {% if (value[1] == currentDate) %}
                                <span data-user-id="{{ user['id'] }}" class="work-status btn {% if day['button'] === 'Start' %} btn-success {% else %} btn-danger {% endif %} pull-right">
                                    {{ day['button'] }}
                                </span>
                            {% endif %}
                            <div id={{ day['id'] }}>
                                {% for time in day['times'] %}
                                    <br><input data-time-id="{{ time['id'] }}" data-type="start" class="time-input" value="{{ time['start'] }}"> - <input data-time-id="{{ time['id'] }}" data-type="end" class="time-input" value="{{ time['end'] }}">
                                {% endfor %}
                            </div>
                            <hr>
                            <p>{{ day['total'] }}</p>
                        {% endif %}
                    {% endfor %}
                </td>
            {% endfor %}
        </tr>
    {% endfor %}
</table>
<script type="text/javascript">
    $(function () {

        $('.time-input').on('change', function () {
            let data = $(this).data();
            let value = $(this).val();

            if(!value.match("^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")) {
                swal.fire({
                    type: 'error',
                    title: 'Ошибка!Введенные данные не соответствуют формату часов!',
                    timer: 5000
                });
            } else {
                data.value = value;
                $.ajax({
                    method: "POST",
                    url: "/admin/time/edit",
                    data: data
                }).done(function () {
                    swal.fire({
                        type: 'success',
                        title: 'Время успешно изменено',
                        showConfirmButton: false,
                        timer: 1500
                    });
                }).fail(function () {
                    swal.fire({
                        type: 'error',
                        title: 'Ошибка!Что то пошло не так!',
                        timer: 5000
                    });
                });
            }

        });

        let form = $('#staff_search');
        form.on('change', function () {

            let month = $('#month').val();
            let year = $('#year').val();
            $.ajax({
                method: "POST",
                url: "/admin/staffControl",
                data: {
                    month: month,
                    year: year
                }
            }).done(function () {
                location.reload()
            }).fail(function () {
                alert('Извините, произошла ошибка!')
            });
        })
    });
</script>