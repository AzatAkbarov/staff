{#{{ content() }}#}

<input class="btn btn-success" id="create_holiday" type="button" value="Создать праздник">

<table id="holidays_table" class="table table-bordered table-striped" align="center">
    <tr>
        <th>Название праздника</th>
        <th>Дата</th>
        <th>Ежегодность</th>
        <th>Действие</th>
    </tr>
    {% for holiday in holidays %}
        <tr id="{{ holiday.id }}">
            <td>{{ holiday.title }}</td>
            <td>{{ holiday.date }}</td>
            <td>{{ holiday.is_every_year }}</td>
            <td><input data-holiday-id="{{ holiday.id }}" class="btn btn-danger pull-right  delete-holiday" type="button" value="Удалить"></td>
        </tr>
    {% else %}
        <p>Извините,но ни одного праздника еще не создано</p>
    {% endfor %}
</table>

<style>
    .ui-datepicker-trigger {
        width: 30px;
    }

    #date {
        width: 260px;
        display: inline-block;
        margin-right: 5px;
    }
</style>
<script>
    $(function () {
        $('#create_holiday').on('click', function () {
            createHoliday()
        });

        $('.delete-holiday').on('click', function () {
            deleteHoliday($(this))
        });
    });
</script>
<script type="text/javascript">
    function deleteHoliday(button) {
        let id = button.data('holiday-id');
        $.ajax({
            method: "GET",
            url: `/admin/holiday/delete/${id}`
        }).done(function (response) {
            let el = $(`#${response.id}`);
            el.detach();
            swal.fire({
                type: 'success',
                title: 'Праздник успешно удален!',
                showConfirmButton: false,
                timer: 1500
            });
        }).fail(function () {
            swal.showValidationMessage(
                `Извините,произошла ошибка`
            )
        });
    }
    function createHoliday() {
        let value = 0;
        swal.fire({
            title: `Создание нового праздника`,
            showCloseButton: true,
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Создать',
            cancelButtonText: 'Отмена',
            html:
                `<form>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Название праздника</label>
                        <input type="text" class="form-control" id="title" aria-describedby="emailHelp" placeholder="Введите название">
                      </div>
                      <div class="form-group">
                        <label for="date">Дата праздника</label><br>
                        <input disabled type="text" class="form-control" id="date" placeholder="Кликните на иконку">
                      </div>
                </form>`,
            input: 'checkbox',
            inputValue: value,
            inputPlaceholder:
                'Каждый год',
            preConfirm: function (value) {
                let title = $('#title').val();
                let date = $('#date').val();
                return new Promise(function (resolve) {
                    if (title.length !== 0 && date.length !== 0) {
                        $.ajax({
                            method: "POST",
                            url: "/admin/holiday/create",
                            data: {
                                title: title,
                                date: date,
                                is_every_year: value
                            }
                        }).done(function (response) {
                            let tbody = $('#holidays_table').find($('tbody'));
                            let newtr = $(`<tr id="${response.id}">
                                                   <td>${response.title}</td>
                                                   <td>${response.date}</td>
                                                   <td>${response.status}</td>
                                                   <td><a class="btn btn-danger center" href="localhost:8000/admin/holiday/delete/${response.id}" title="Удалить">Удалить</a></td>
                                           </tr>`);
                            tbody.append(newtr);
                            swal.fire({
                                type: 'success',
                                title: 'Праздник успешно создан!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }).fail(function (response) {
                            swal.showValidationMessage(
                                `Извините,произошла ошибка, ${response.message}`
                            )
                        });
                    } else if (title.length === 0 && date.length === 0) {
                        swal.showValidationMessage(
                            'Заполните поля!'
                        )
                    } else if (title.length === 0) {
                        swal.showValidationMessage(
                            'Введите название!'
                        )
                    } else {
                        swal.showValidationMessage(
                            'Выберите дату!'
                        )
                    }
                })
            },
            onOpen: function () {
                $("#date").datepicker({
                    dateFormat: "d/MM/yy",
                    showOn: "button",
                    buttonImage: "/img/calendar-alt-regular.svg",
                    buttonImageOnly: true,
                    buttonText: "Выберите дату",
                });
            }
        })
    }
</script>