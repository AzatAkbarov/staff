<?php

use Phalcon\Config;

return new Config([
    'privateResources' => [
        'user' => [
            'changePassword',
            'startStopWork'
        ],
        'admin' => [
            'showUsers',
            'createUser',
            'deleteUser',
            'createHoliday',
            'showHolidays',
            'deleteHoliday',
            'staffControl',
            'editTime'
        ]
    ]
]);
