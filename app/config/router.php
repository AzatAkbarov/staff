<?php


// Define your routes here
$router = new Phalcon\Mvc\Router();


$router->add(
    '/',
    [
        'controller' => 'index',
        'action'     => 'index',
    ]
);

$router->add(
    '/login',
    [
        'controller' => 'session',
        'action'     => 'login',
    ]
);

$router->add(
    '/user/change_password',
    [
        'controller' => 'user',
        'action'     => 'changePassword',
    ]
);

$router->add(
    '/user/startStopWork',
    [
        'controller' => 'user',
        'action'     => 'startStopWork',
    ]
);

$router->add(
    '/logout',
    [
        'controller' => 'session',
        'action'     => 'logout',
    ]
);


$router->add(
    '/admin/user/create',
    [
        'controller' => 'admin',
        'action'     => 'createUser',
    ]
);

$router->add(
    '/admin/staff/control',
    [
        'controller' => 'admin',
        'action'     => 'staffControl',
    ]
);

$router->add(
    '/admin/time/edit',
    [
        'controller' => 'admin',
        'action'     => 'editTime',
    ]
);

$router->add(
    '/admin/users/show',
    [
        'controller' => 'admin',
        'action'     => 'showUsers',
    ]
);

$router->add(
    '/admin/user/delete/{id:\d+}',
    [
        'controller' => 'admin',
        'action'     => 'deleteUser',
    ]
);

$router->add(
    '/admin/holiday/create',
    [
        'controller' => 'admin',
        'action'     => 'createHoliday',
    ]
);

$router->add(
    '/admin/holiday/delete/{id:\d+}',
    [
        'controller' => 'admin',
        'action'     => 'deleteHoliday',
    ]
);

$router->add(
    '/admin/holidays/show',
    [
        'controller' => 'admin',
        'action'     => 'showHolidays',
    ]
);

return $router;
